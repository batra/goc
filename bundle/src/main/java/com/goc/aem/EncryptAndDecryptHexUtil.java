package com.goc.aem;

import java.io.UnsupportedEncodingException;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.goc.aem.impl.ForgotPasswordServlet;

public class EncryptAndDecryptHexUtil {

    private static final Logger log = LoggerFactory.getLogger(EncryptAndDecryptHexUtil.class);
    
	public static String encode(String uniqueID)
	{
		String encoded = null;
		encoded = Hex.encodeHexString(uniqueID.getBytes());
		System.out.println("Encoded: "+encoded);
		return encoded;
	}
	
	public static String decode(String encoded)
	{
		String decoded = "";
		try {
			byte[] decodedBytes = Hex.decodeHex(encoded.toString().toCharArray());   
			decoded = new String(decodedBytes, new Hex().getCharsetName());
			System.out.println("Decoded: "+decoded);
		} 
		catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (DecoderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return decoded;
	}
}

