package com.goc.aem;

/**
 * Created by batra on 2/6/2015.
 */
public interface EmailService {

	public void sendEmail();

	public void sendEmailToUser( String recipientEmailAddress, String recipientName);
}
