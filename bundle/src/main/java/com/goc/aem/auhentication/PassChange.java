package com.goc.aem.auhentication;

import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.ModificationItem;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

public class PassChange {

	public static void changePasswordAD(String dn, String password) {
	
		// build modification request to set the attribute
		ModificationItem[] mods = new ModificationItem[1];
	
		// init object
		BasicAttribute attr = null;
		
		// set the user account control
		attr = new BasicAttribute("userAccountControl", "512");
		mods[0] = new ModificationItem(LdapContext.REPLACE_ATTRIBUTE, attr);

		// encode the password		
		String quotedPassword = "\"" + password + "\"";
		char unicodePwd[] = quotedPassword.toCharArray();
		byte pwdArray[] = new byte[unicodePwd.length * 2];
		for (int i = 0; i < unicodePwd.length; i++) {
			pwdArray[i * 2 + 1] = (byte) (unicodePwd[i] >>> 8);
			pwdArray[i * 2 + 0] = (byte) (unicodePwd[i] & 0xff);
		}
		mods[1] = new ModificationItem(LdapContext.REPLACE_ATTRIBUTE, new BasicAttribute("UnicodePwd", pwdArray));

		// get the ldap context
		LdapContext ctx = getADCon();

		try {
			ctx.modifyAttributes(dn, mods);
		} catch (NamingException e) {
			e.printStackTrace();
		}

		try {
			ctx.close();
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static LdapContext getADCon() {

		// attributes for the connection
		String user = ""; // domain admin account
		String pass = ""; // password for the account
		String host = ""; // host of the domain controller
		String port = "389"; // 389 is non SSL
		String base = ""; // base DN of the users container
	
		// init the object and hashtable for attributes
		LdapContext ctx = null;
		Hashtable<String, String> env = new Hashtable<String, String>();
	
		// set security credentials, note using simple cleartext authentication
		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		env.put(Context.SECURITY_PRINCIPAL, user);
		env.put(Context.SECURITY_CREDENTIALS, pass);
	
		// context factory
		env.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");
		// connect to my domain controller
		env.put(Context.PROVIDER_URL, "ldap://" + host + ":" + port);
		try {
			ctx = new InitialLdapContext(env, null);
		} catch (NamingException e) {
			// failed to get a context
			e.printStackTrace();
			return null;
		}
		return ctx;
	}
}