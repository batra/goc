package com.goc.aem.impl;

import com.goc.aem.EmailService;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

@SlingServlet(
		label = "ForgotPasswordServlet - Sling All Methods Servlet",
		description = "ForgotPasswordServlet - Sample implementation of a Sling All Methods Servlet.",
		//paths = { "/services/forgotpwd" },
		methods = { "GET", "POST" }, // Ignored if paths is set - Defaults to GET if not specified
		resourceTypes = {"/apps/goc/components/page/contentpage" }, // Ignored if paths is set
		selectors = { "forgot" }, // Ignored if paths is set
		extensions = { "html", "htm" }  // Ignored if paths is set
)
public class ForgotPasswordServlet extends SlingAllMethodsServlet {

	private static final Logger log = LoggerFactory.getLogger(ForgotPasswordServlet.class);


	@Reference
	private transient EmailService emailService;

	/**
	 * Add overrides for other SlingAllMethodsServlet here (doHead, doTrace, doPut, doDelete, etc.)
	 */

	@Override
	protected final void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {
		// Implement custom handling of GET requests

		log.info("###################### inside ForgotPasswordServlet#doGet() ############### ");

		String username = request.getParameter("username");
		log.info("Username = "+ username);
		boolean success = true; ///false

		final JSONArray jsonArray = new JSONArray();
		final JSONObject jsonObject = new JSONObject();

			try {
				// Integrate ADFS code here.
				//TODO

				//for success
				if(success) {
					jsonObject.put("success", "true");
					jsonObject.put("message", "Request send, You will receive an email shortly.");

					// send email to user.
					//emailService.sendEmail(); // this one is only for test
					//TODO put the valid email and username here.
					emailService.sendEmailToUser("user@gmail.com", "username");

				}if(! success){
					jsonObject.put("success", "false");
					jsonObject.put("message", "Invalid username, Please try again.");
				}
				jsonArray.put(jsonObject);

			} catch (JSONException e) {
				log.info("#########JSONException in doGet() ",e);
			}


		// change the response type to json
		response.setHeader("Content-Type", "application/json");
		response.getWriter().print(jsonObject);
		//response.getWriter().write("<html><head></head><body>Thanks "+ "!</body></html>");
		// By Default the 200 HTTP Response status code is used; below explicitly sets it.
		response.setStatus(SlingHttpServletResponse.SC_OK);
		//log.info("request.getRequestURI()= "+request.getRequestURI());
		//response.sendRedirect(request.getRequestURI());
		response.sendRedirect("/content/forgot-password.html?success="+success);
	}

	@Override
	protected final void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {
		// Implement custom handling of POST requests

		log.info("###################### inside ForgotPasswordServlet#doPost() ############### ");

		String username = request.getParameter("username");
		log.info("Username = "+ username);

		// Set the response type; this might be JSON, etc.
		// The repsonse type is usually closely correlated to the extension the servlet listens on
		response.setContentType("text/html");

		response.getWriter().write("<html><head></head><body>Thanks "
				+ "!</body></html>");
		// By Default the 200 HTTP Response status code is used; below explicitly sets it.
		response.setStatus(SlingHttpServletResponse.SC_OK);



	}


}
