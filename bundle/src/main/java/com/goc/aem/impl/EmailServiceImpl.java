package com.goc.aem.impl;

import com.adobe.acs.commons.email.EmailService;
import com.goc.aem.HelloService;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.goc.aem.EncryptAndDecryptHexUtil;

/**
 * One implementation of the {@link HelloService}. Note that
 * the repository is injected, not retrieved.
 */
@Service
@Component(label = "HelloServiceImpl  Service",immediate = false,metatype = true)
public class EmailServiceImpl implements com.goc.aem.EmailService {

	private static final Logger log = LoggerFactory.getLogger(EmailServiceImpl.class);

	@Reference
	private EmailService emailService;

	@Reference
	private ConfigurationAdmin configurationAdmin;


	public void sendEmail(){

		String recipientEmailAddress =  "test@adobe.com";
		String recipientName = "testUser";
		sendEmailToUser(recipientEmailAddress, recipientName);

	}

	public void sendEmailToUser( String recipientEmailAddress, String recipientName){
	    String encryptedRecipientName = EncryptAndDecryptHexUtil.encode(recipientName);
		// Get the OSGi Email Service
		//EmailService emailService = sling.getService(EmailService.class);

		// Specify the template file to use (this is an absolute path in the JCR)
		String templatePath = "/etc/notification/email/default/com.goc.aem/en.txt";
		String resetPagePath = "/content/reset-password.html";

		//adding username param
		if(!recipientName.isEmpty()){
			resetPagePath+= "?username="+encryptedRecipientName;
		}
		log.info("resetPagePath #########= "+ resetPagePath);

		// finalResetPagePath = http://localhost:4502/content/reset-password.html?username=abc

		//Set the dynamic vaiables of your email template
		Map<String, String> emailParams = new HashMap<String,String>();
		emailParams.put("message","Your message goes here. ");
		emailParams.put("resetPagePath",resetPagePath);
		emailParams.put("recipientName",recipientName);

		//  Customize the sender email address - if required
		emailParams.put("senderEmailAddress","no-reply@adobe.com");
		emailParams.put("senderName","No-Reply");

		// host config parameters.
		emailParams.putAll(getEmailNotificationServiceConfigs());

		// Array of email recipients
		String[] recipients = new String[1];
		recipients[0] = recipientEmailAddress;

		// emailService.sendEmail(..) returns a list of all the recipients that could not be sent the email
		// An empty list indicates 100% success
		List<String> failureList = emailService.sendEmail(templatePath, emailParams, recipients);

		if (failureList.isEmpty()) {
			log.info("Email sent successfully to the recipients");
		} else {
			log.info("Email sent failed");
		}

	}


	/**
	 * This methods returns the serviceConfig map for
	 * EMailNotificationService which contains information like
	 * host.prefix and from.address.
	 *
	 * @return serviceConfig
	 */
	private Map<String, String> getEmailNotificationServiceConfigs() {
		Map<String, String> serviceConfig = new HashMap<String, String>();
		String pid = "com.day.cq.workflow.impl.email.EMailNotificationService";
		try {
			Configuration configuration = configurationAdmin.getConfiguration(pid);
			if (configuration != null) {
				String hostPrefix = (String) configuration.getProperties().get("host.prefix");
				serviceConfig.put("host.prefix", hostPrefix);
				String fromAddress = (String) configuration.getProperties().get("from.address");
				serviceConfig.put("from.address", fromAddress);
			}
		} catch (IOException e) {
			log.info("IOException in reading service configuration.");
		}
		return  serviceConfig;
	}

}
