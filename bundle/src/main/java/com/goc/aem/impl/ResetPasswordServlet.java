package com.goc.aem.impl;

import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

@SlingServlet(
		label = "ResetPasswordServlet - Sling All Methods Servlet",
		description = "ResetPasswordServlet - Sample implementation of a Sling All Methods Servlet.",
		//paths = { "/services/resetpwd" },
		methods = { "GET", "POST" }, // Ignored if paths is set - Defaults to GET if not specified
		resourceTypes = { "/apps/goc/components/page/resetpage"}, // Ignored if paths is set
		selectors = { "reset" }, // Ignored if paths is set
		extensions = { "html", "htm" }  // Ignored if paths is set
)
public class ResetPasswordServlet extends SlingAllMethodsServlet {

	private static final Logger log = LoggerFactory.getLogger(ForgotPasswordServlet.class);

	/**
	 * Add overrides for other SlingAllMethodsServlet here (doHead, doTrace, doPut, doDelete, etc.)
	 */

	@Override
	protected final void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {
		// Implement custom handling of GET requests

		log.info("###################### inside ResetPasswordServlet#doGet() ############### ");

		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String confirmpassword = request.getParameter("confirm-password");
		log.info("Username = "+ username);
		log.info("pwd = "+ password + " "+ confirmpassword);

		final JSONArray jsonArray = new JSONArray();
		final JSONObject jsonObject = new JSONObject();

		boolean success = true; ///false

		try {
			// Integrate ADFS code here.
			//TODO upon successful password update. Set success = true.

			//for success
			if(success) {
				jsonObject.put("success", "true");
				jsonObject.put("message", "Password reset successfully");
			}if(! success){
				jsonObject.put("success", "false");
				jsonObject.put("message", "Password reset not successful. Please try again.");
			}

			jsonArray.put(jsonObject);

		} catch (JSONException e) {
			log.info("#########JSONException in doGet() ",e);
		}


		// change the response type to json
		response.setHeader("Content-Type", "application/json");

		response.getWriter().print(jsonObject);

		// By Default the 200 HTTP Response status code is used; below explicitly sets it.
		response.setStatus(SlingHttpServletResponse.SC_OK);

		response.sendRedirect("/content/reset-password.html?success="+success);

	}

	@Override
	protected final void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {
		// Implement custom handling of POST requests

		log.info("###################### inside ResetPasswordServlet#doPost() ############### ");

		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String confirmpassword = request.getParameter("confirm-password");
		log.info("Username = "+ username);
		log.info("pwd = "+ password + " "+ confirmpassword);


		// Set the response type; this might be JSON, etc.
		// The repsonse type is usually closely correlated to the extension the servlet listens on
		response.setContentType("text/html");

		response.getWriter().write("<html><head></head><body>Thanks "
				+ "!</body></html>");
		// By Default the 200 HTTP Response status code is used; below explicitly sets it.
		response.setStatus(SlingHttpServletResponse.SC_OK);


	}


}
