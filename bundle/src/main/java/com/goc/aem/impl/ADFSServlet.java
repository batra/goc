package com.goc.aem.impl;


import org.apache.felix.scr.annotations.*;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * %%
 * Author   : ADOBE
 * Date     : June 5, 2014
 * %%
 */


@Component(
		label = "GOC -  ADFS Servlet",
		description = "Provides a Raw HTML response.",
		immediate = false,
		metatype = true
)
@Service
@Properties({@Property(name = "sling.servlet.paths", value = "/services/adfs", propertyPrivate = true),
		@Property(name = "sling.servlet.extensions", value = "html", propertyPrivate = true)})
public class ADFSServlet extends SlingSafeMethodsServlet {

	/**
	 * logger instance for the service.
	 */
	private static final Logger log = LoggerFactory.getLogger(ADFSServlet.class);

	@Override
	protected final void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {
		// Implement custom handling of GET requests

		log.info("###################### inside ADFSServlet#doGet() ############### ");

		// Set the response type; this might be JSON, etc.
		// The repsonse type is usually closely correlated to the extension the servlet listens on
		response.setContentType("text/html");

		// Do some work
		Resource resource = request.getResourceResolver().getResource("/content/geometrixx");
		ValueMap properties = resource.adaptTo(ValueMap.class);
		if (properties != null) {
// Writing HTML in servlets is usually inadvisable, and is better suited to be provided via a JSP/Sightly template
// This is just an example.
			response.getWriter().write("<html><head></head><body>Hello "
					+ properties.get("name", "World")
					+ "!</body></html>");
// By Default the 200 HTTP Response status code is used; below explicitly sets it.
			response.setStatus(SlingHttpServletResponse.SC_OK);
		} else {
// Set HTTP Response Status code appropriately
			response.setStatus(SlingHttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			response.getWriter().write("ERROR");
		}

	}


}