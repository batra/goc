<%--

ADOBE CONFIDENTIAL
__________________

Copyright 2012 Adobe Systems Incorporated
All Rights Reserved.

NOTICE:  All information contained herein is, and remains
the property of Adobe Systems Incorporated and its suppliers,
if any.  The intellectual and technical concepts contained
herein are proprietary to Adobe Systems Incorporated and its
suppliers and are protected by trade secret or copyright law.
Dissemination of this information or reproduction of this material
is strictly forbidden unless prior written permission is obtained
from Adobe Systems Incorporated.
--%>
<%@page session="false"
        contentType="text/html"
        pageEncoding="utf-8"
        import="java.util.HashMap,
                  java.util.Map,
                  java.util.Iterator,
                  org.apache.commons.io.IOUtils,
                  org.apache.commons.lang3.StringUtils,
                  org.apache.sling.api.resource.Resource,
                  org.apache.sling.api.resource.ResourceUtil,
                  org.apache.sling.api.resource.ValueMap,
                  com.adobe.granite.xss.XSSAPI,
                  com.day.cq.i18n.I18n,
                  com.day.cq.widget.HtmlLibrary,
                  com.day.cq.widget.HtmlLibraryManager,
                  com.day.cq.widget.LibraryType,
                  org.apache.sling.auth.core.AuthUtil"%><%
%><%@taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0"%><%
%><%@ taglib prefix="ui" uri="http://www.adobe.com/taglibs/granite/ui/1.0" %><%!

    static final String PARAM_NAME_REASON = "j_reason";

    static final String REASON_KEY_INVALID_LOGIN = "invalid_login";
    static final String REASON_KEY_SESSION_TIMED_OUT = "session_timed_out";

    String printProperty(ValueMap cfg, I18n i18n, XSSAPI xssAPI, String name, String defaultText) {
        String text = cfg.get(name, String.class);
        return xssAPI.encodeForHTML( text != null ? i18n.getVar(text) : defaultText );
    }

    /**
     * Select the configuration root resource among those stored under <code>configs</code> node.
     * The configuration with the highest order property is selected.
     * @param current the
     * @return the selected configuration root resource or <code>null</code> if no configuration root could be found.
     */
    Resource getConfigRoot(Resource current) {
        Resource configs = current.getChild("configs");
        Resource configRoot = null;
        if (configs != null) {
            long maxOrder = Long.MIN_VALUE;
            for (Iterator<Resource> cfgs = configs.listChildren() ; cfgs.hasNext() ; ) {
                Resource cfg = cfgs.next();
                ValueMap props = ResourceUtil.getValueMap(cfg);
                Long order = props.get("order", Long.class);
                if (order != null) {
                    if (order > maxOrder) {
                        configRoot = cfg;
                        maxOrder = order;
                    }
                }
            }
        }
        return configRoot;
    }

%><sling:defineObjects /><%

    final Resource configs = getConfigRoot(resource);

    final I18n i18n = new I18n(slingRequest);
    final XSSAPI xssAPI = sling.getService(XSSAPI.class).getRequestSpecificAPI(slingRequest);
    final ValueMap cfg = ResourceUtil.getValueMap(configs);

    final String authType = request.getAuthType();
    final String user = request.getRemoteUser();
    final String contextPath = slingRequest.getContextPath();

    // used to map readable reason codes to valid reason messages to avoid phishing attacks through j_reason param
    Map<String,String> validReasons = new HashMap<String, String>();
    validReasons.put(REASON_KEY_INVALID_LOGIN, printProperty(cfg, i18n, xssAPI, "box/invalidLoginText", i18n.get("User name and password do not match")));
    validReasons.put(REASON_KEY_SESSION_TIMED_OUT, printProperty(cfg, i18n, xssAPI, "box/sessionTimedOutText", i18n.get("Session timed out, please login again")));

    String reason = request.getParameter(PARAM_NAME_REASON) != null
            ? request.getParameter(PARAM_NAME_REASON)
            : "";

    if (!StringUtils.isEmpty(reason)) {
        if (validReasons.containsKey(reason)) {
            reason = validReasons.get(reason);
        } else {
            // a reason param value not matching a key in the validReasons map is considered bogus
            log.warn("{} param value '{}' cannot be mapped to a valid reason message: ignoring", PARAM_NAME_REASON, reason);
            reason = "";
        }
    }

%><!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6 oldie" class="coral-App"> <![endif]-->
<!--[if IE 7 ]> <html class="ie7 oldie" class="coral-App"> <![endif]-->
<!--[if IE 8 ]> <html class="ie8 oldie" class="coral-App"> <![endif]-->
<!--[if !(lt IE 9)|!(IE)]><!--> <html class="coral-App"> <!--<![endif]-->
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <%-- optimized for mobile, zoom/scaling disabled --%>
    <meta name="viewport" content="width = device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="chrome=1" />
    <title><%= printProperty(cfg, i18n, xssAPI, "title", i18n.get("Adobe Marketing Cloud")) %></title>
    <style type="text/css">
        <%
            HtmlLibraryManager htmlMgr = sling.getService(HtmlLibraryManager.class);
            HtmlLibrary lib = htmlMgr.getLibrary(LibraryType.CSS, "/libs/granite/core/content/login/clientlib");
            IOUtils.copy(lib.getInputStream(true), out, "utf-8");
        %>
    </style>
    <ui:includeClientLib css="coralui2" />
    <%
        String favicon = xssAPI.getValidHref(cfg.get("favicon", "login/adobe-logo.png"));
        favicon = xssAPI.getValidHref(favicon);
    %>
    <link rel="shortcut icon" href="<%= favicon %>" type="image/png">
    <link rel="icon" href="<%= favicon %>" type="image/png">
    <%-- Load the clientlib(s). Extension libraries should use the  'granite.core.login.extension' category. --%>
    <ui:includeClientLib js="jquery,typekit,granite.core.login,granite.core.login.extension"/>
</head>
<body class="coral--light coral-App">
<div id="wrap">
    <div id="backgrounds">
        <%-- this holds all the background divs that are dynamically loaded --%>
        <div id="bg_default" class="background"></div>
    </div>
    <div id="tag"></div>

    <%
        // make sure the redirect path is valid and prefixed with the context path
        String redirect = request.getParameter("resource");
        if (redirect == null || !AuthUtil.isRedirectValid(request, redirect)) {
            redirect = "/";
        }
        if (!redirect.startsWith(contextPath)) {
            redirect = contextPath + redirect;
        }
        String urlLogin = request.getContextPath() + resource.getPath() + ".html/j_security_check";

        if (authType == null || user == null || user.equals("anonymous")) {

    %>
    <div id="login-box">
        <div class="header">
            <h1 class="coral-Heading coral-Heading--1"><%= printProperty(cfg, i18n, xssAPI, "box/title", i18n.get("Welcome to Adobe Marketing Cloud")) %></h1>
        </div>
        <div id="leftbox" class="box">
            <p>
                <%= printProperty(cfg, i18n, xssAPI, "box/text", i18n.get("All the tools you need to solve these complex digital business challenges.")) %>
                <a class="coral-Link" id="learnmore" href="<%= xssAPI.getValidHref(i18n.getVar(cfg.get("box/learnMore/href", "#"))) %>" x-cq-linkchecker="skip"><%= printProperty(cfg, i18n, xssAPI, "box/learnMore/text", i18n.get("Learn More")) %></a>
            </p>
        </div>
        <div id="rightbox" class="box">
            <% String autocomplete = cfg.get("box/autocomplete", false) ? "on" : "off" ; %>
            <form class="coral-Form coral-Form--vertical" name="login" method="POST" id="login" action="<%= xssAPI.getValidHref(urlLogin) %>" novalidate="novalidate">
                <input type="hidden" name="_charset_" value="UTF-8"/>
                <input type="hidden" name="errorMessage" value="<%= validReasons.get(REASON_KEY_INVALID_LOGIN) %>"/>
                <input type="hidden" name="resource" value="<%= xssAPI.encodeForHTMLAttr(redirect) %>"/>
                <p class="sign-in-title"><%= printProperty(cfg, i18n, xssAPI, "box/formTitle", i18n.get("Sign in")) %></p>
                <%
                    String userPlaceholder = printProperty(cfg, i18n, xssAPI, "box/userPlaceholder", i18n.get("User name"));
                    String passwordPlaceholder = printProperty(cfg, i18n, xssAPI, "box/passwordPlaceholder", i18n.get("Password"));
                %>
                <label for="username"><span><%= userPlaceholder %></span></label>
                <input class="coral-Form-field coral-Textfield" id="username" name="j_username" type="text" autofocus="autofocus" pattern=".*" placeholder="<%= userPlaceholder %>" spellcheck="false" autocomplete="<%= autocomplete %>"/>
                <label for="password"><span><%= passwordPlaceholder %></span></label>
                <input class="coral-Form-field coral-Textfield" id="password" name="j_password" type="password"  placeholder="<%= passwordPlaceholder %>" spellcheck="false" autocomplete="<%= autocomplete %>"/>
                <div id="error" class="coral-Form-field coral-Alert coral-Alert--error <%= reason.length() > 0 ? "" : "hidden" %>">
                    <i class="coral-Alert-typeIcon coral-Icon coral-Icon--sizeS coral-Icon--alert"></i>
                    <div class='coral-Alert-message'><%= reason %></div>
                </div>
                <button type="submit" class="coral-Button coral-Button--primary"><%= printProperty(cfg, i18n, xssAPI, "box/submitText", i18n.get("Sign In")) %></button>
                <a class="coral-Link" id="forgot-pwd" href="/content/forgot-password.html" x-cq-linkchecker="skip"><%= printProperty(cfg, i18n, xssAPI, "box/forgotPwd/text", i18n.get("Forgot Password")) %></a>

            </form>
        </div>
    </div>
    <div id="push"></div>
</div>
<div id="footer">
    <div class="legal-footer"><%
        // Footer: default copyright (removable)
        if (cfg.containsKey("footer/copy/text")) {
            %><span><%= printProperty(cfg, i18n, xssAPI, "footer/copy/text", "") %></span><%
        }
        %><ul id="usage-box">
            <%

                // Footer: dynamic items (config/footer/items)
                if (configs.getChild("footer/items") != null) {
                    Iterator<Resource> footerItems = configs.getChild("footer/items").listChildren();
                    while (footerItems.hasNext()) {
            %><li><%
            String itemName = footerItems.next().getName();
            String href = i18n.getVar(cfg.get("footer/items/" + itemName + "/href", String.class));
            if (href != null) {
        %><a href="<%= xssAPI.getValidHref(href) %>"><%
            }
        %><%= printProperty(cfg, i18n, xssAPI, "footer/items/" + itemName + "/text", "") %><%
            if (href != null) {
        %></a><%
            }
        %></li><%
                }
            }

        %>
        </ul>
    </div>
</div>
<script type="text/javascript">
    // try to append the current hash/fragment to the redirect resource
    if (window.location.hash) {
        var resource = document.getElementById("resource");
        if (resource) {
            resource.value += window.location.hash;
        }
    }
</script>
<% } else { %>
<script type="text/javascript">
    var redirect = '<%= xssAPI.encodeForJSString(xssAPI.getValidHref(redirect)) %>';
    if (window.location.hash) {
        redirect += window.location.hash;
    }
    document.location = redirect;
</script>
<% } %>
<!-- QUICKSTART_HOMEPAGE - (string used for readyness detection, do not remove) -->
</body>
</html>