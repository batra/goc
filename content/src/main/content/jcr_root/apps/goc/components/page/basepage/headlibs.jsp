<%--
  Copyright 1997-2010 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  Includes the scripts and css to be included in the head tag

  ==============================================================================

--%><%@ page session="false"
        import="java.util.HashMap,
                  java.util.Map,
                  java.util.Iterator,
                  org.apache.commons.io.IOUtils,
                  org.apache.commons.lang3.StringUtils,
                                    org.apache.sling.api.resource.Resource,
                                    org.apache.sling.api.resource.ResourceUtil,
                                    org.apache.sling.api.resource.ValueMap,
                                    com.adobe.granite.xss.XSSAPI,
                                    com.day.cq.i18n.I18n,
                                    com.day.cq.widget.HtmlLibrary,
                                    com.day.cq.widget.HtmlLibraryManager,
                                    com.day.cq.widget.LibraryType,
                                    org.apache.sling.auth.core.AuthUtil"
 %><%
%><%@include file="/libs/foundation/global.jsp" %><%
%><cq:includeClientLib categories="cq.foundation-main"/>
<%@ taglib prefix="ui" uri="http://www.adobe.com/taglibs/granite/ui/1.0" %>
<cq:include script="/libs/cq/cloudserviceconfigs/components/servicelibs/servicelibs.jsp"/><%
    currentDesign.writeCssIncludes(pageContext); %>

    <sling:defineObjects /><%


        final I18n i18n = new I18n(slingRequest);

        %>

    <style type="text/css">
            <%
                HtmlLibraryManager htmlMgr = sling.getService(HtmlLibraryManager.class);
                HtmlLibrary lib = htmlMgr.getLibrary(LibraryType.CSS, "/libs/granite/core/content/login/clientlib");
                IOUtils.copy(lib.getInputStream(true), out, "utf-8");
            %>
        </style>
        <ui:includeClientLib css="coralui2" />