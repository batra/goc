
<%
%><%@include file="/libs/foundation/global.jsp"%><%
%><%@page session="false" 
    import="com.day.cq.i18n.I18n" %><%
%><%
	// TODO add you code here
%>
<%
        final I18n i18n = new I18n(slingRequest);

        %>

    <div id="wrap">
        <div id="backgrounds">
            <%-- this holds all the background divs that are dynamically loaded --%>
            <div id="bg_default" class="background"></div>
        </div>


        <div id="login-box">
            <div class="header">
                <h1 class="coral-Heading coral-Heading--1">
                    <%=  i18n.get("Forget Password") %>
                </h1>
            </div>
            <div id="leftbox" class="box">
                <p>
                    <%= i18n.get("All the tools you need to solve these complex digital business challenges.") %>

                </p>
            </div>

            <div id="rightbox" class="box">
                <form name="forgotpassword" class="coral-Form coral-Form--vertical" action="<%=currentPage.getPath()%>.forgot.html" id="forgotpassword" method="GET" >
                    <p class="sign-in-title">Forgot Password</p>

                     <input class="coral-Form-field coral-Textfield"  name="username" type="text" id="username"
                     autofocus="autofocus"  placeholder="<%= i18n.get("User Name") %>" spellcheck="false"
                     autocomplete="false"/>

                    <div id="error" class="coral-Form-field coral-Alert coral-Alert--error hidden">
                        <i class="coral-Alert-typeIcon coral-Icon coral-Icon--sizeS coral-Icon--alert"></i>
                        <div class="coral-Alert-message"></div>
                    </div>



                    <%if("true".equalsIgnoreCase(request.getParameter("success"))){%>

                        <div class="coral-Alert coral-Alert--Success">
                           <i class="coral-Alert-typeIcon coral-Icon coral-Icon--sizeS coral-Icon--checkCircle"></i>
                           <strong class="coral-Alert-title">Success</strong>
                           <div class="coral-Alert-message">Request send, You will receive an email shortly.</div>
                        </div>

                    <%}else if("false".equalsIgnoreCase(request.getParameter("success"))){%>

                    	<button type="submit" class="coral-Button coral-Button--primary">
                            <%= i18n.get("Submit") %>
                         </button>
						 <br><br>
                    	<div id="error" class="coral-Form-field coral-Alert coral-Alert--error">
                            <i class="coral-Alert-typeIcon coral-Icon coral-Icon--sizeS coral-Icon--alert"></i>
                            <strong class="coral-Alert-title">Error</strong>
                            <div class="coral-Alert-message">Error while submitting response. Please try again.</div>
                        </div> 

                    <%}else{%>
                    	<button type="submit" class="coral-Button coral-Button--primary">
                            <%= i18n.get("Submit") %>
                         </button>

                    <%}%>

                  </form>

            </div>

        </div>

    </div>

<script type="text/javascript">

    $("#forgotpassword").submit(function(){

        var $error = $("#error");
        var $msgDiv = $error.find(".coral-Alert-message");
		var username = $("#username").val();

        if(!username || username.trim()==''){
            $error.removeClass("hidden");
            $msgDiv.html("Username Required")
            return false;
        }

        return true;

    });


</script>